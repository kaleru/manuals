*******************
Changing Text Color
*******************

You can change a text's color the same way as you change the color of any vector
object in Inkscape. First, you need to select the text, or, using the Text tool,
you can even select words or characters in a text, then you click on the color
of your choice. The text can have a separate paint for fill and stroke, like all
the objects in Inkscape. You can also apply a gradient to a text.

.. figure:: images/text_color.png
   :alt: Text variations
   :class: screenshot

   Text variations
