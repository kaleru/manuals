*************************
Finding Help for Inkscape
*************************

Inkscape has a helpful and active community. If you find yourself stuck, want to ask for help or simply wish to connect with others, consider joining `the chat <https://chat.inkscape.org/channel/inkscape_user>`__ or `the forum <https://inkscape.org/forums/>`__.

If you want to read up on more advanced features of Inkscape, `Inkscape: Guide to a Vector Drawing Program by Tavmjong Bah <http://tavmjong.free.fr/INKSCAPE/>`__ (currently covering Inkscape 0.92) is the standard textbook on it. There is also a series of `tutorials <https://inkscape.org/learn/tutorials/>`__ on Inkscape's homepage.

The `keyboard and mouse reference <https://inkscape.org/doc/keys.html>`__  provides an overview of tool and dialog shortcuts and can help you speed up your workflow.

Taking apart other people's work can be a good way to learn something. Maybe you find inspiration while browsing Inkscape's `gallery <https://inkscape.org/gallery/>`__.

Lastly, if you're reading the PDF version of this guide, check out `Read the Docs <https://inkscape-manuals.readthedocs.io/en/latest/>`__ for updates to it.
