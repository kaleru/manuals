***************
The Shape Tools
***************

Geometric shapes, despite their simple look, are useful for many drawing
techniques (and not only vector drawing). With path operations (Union,
Difference, etc…), you can quickly get awesome results from some
elementary shapes. You can even further improve that with path tools.
Both path operations and path tools are detailed in later sections.

.. figure:: images/shape_tools_icons.png
    :alt: The icons of the Rectangle, Star, Ellipse and Spiral tools
    :class: screenshot

Let's draw some geometric shapes. All the shape tools follow similar
rules to let you create and edit your shapes. But each tool has specific
options: for example, a circle can become a pie wedge, or a rectangle
can have its corners rounded.

To create a geometric shape:

-  enable the relevant tool in the toolbar by clicking on it;
-  on the canvas, press the mouse button and hold while you drag the mouse;
-  release the mouse button to display the shape.

.. figure:: images/create_rect.png
    :alt: A rectangle is created by clicking and dragging the mouse
    :class: screenshot

    Click and drag to create a shape.

Once the mouse is released and the shape is displayed, various handles
will become visible. Many of Inkscape's tools use handles
for one purpose or another. But it's the handles of the geometric shapes
which are used for creating many fancy and exciting effects. The handles
may be tiny circles, squares and/or diamonds. Sometimes, the same
handles can be available for different tools. We will learn more about
each handle's function in the following chapters.

.. figure:: images/create_shapes.png
    :alt: An ellipse, a rectangle and a star
    :class: screenshot

    Primitive shapes provide handles as squares, circles or diamonds.

Many features of Inkscape are accessible through keyboard shortcuts, and
sometimes even *only* through key shortcuts. While drawing your shape:

-  press :kbd:`Ctrl` while you drag the mouse, with the Rectangle and
   Ellipse tools, to create **squares** and **circles**;
-  press :kbd:`Shift` while you drag to draw from the center, rather
   than from one corner.

Try drawing some shapes, with and without those keys to get an idea of
how they can be used.

.. figure:: images/ellipses-cloud-example.png
    :alt: Several ellipses are combined into a cloud
    :class: screenshot

    Sketching a cloud from ellipses.
